package org.typo3.typoscript;


import com.intellij.lang.Language;
import com.intellij.psi.templateLanguages.TemplateLanguage;

public class TypoScriptLanguage extends Language implements TemplateLanguage
{
	public static final TypoScriptLanguage INSTANCE = new TypoScriptLanguage();

	public TypoScriptLanguage()
	{
		super("TypoScript", "text/typoscript");
	}
}
