package org.typo3.typoscript.actions;

import com.intellij.openapi.diagnostic.Logger;
import com.intellij.psi.PsiDirectory;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import org.jetbrains.annotations.NotNull;
import org.typo3.typoscript.TypoScriptBundle;
import org.typo3.typoscript.icons.TypoScriptIcons;

/**
 * Created with IntelliJ IDEA.
 * User: sok
 * Date: 08.04.12
 * Time: 14:17
 * To change this template use File | Settings | File Templates.
 */
public class NewTypoScriptFileAction extends NewTypoScriptActionBase
{
	private static final Logger log = Logger.getInstance("#NewActionBase");

	public NewTypoScriptFileAction()
	{
		super("New TypoScript file", "Create a new TypoScript file", TypoScriptIcons.FILE_TYPE_ICON_16);
	}

	protected String getDialogPrompt()
	{
		return TypoScriptBundle.message("newfile.dialog.prompt");
	}

	protected String getDialogTitle()
	{
		return TypoScriptBundle.message("newfile.dialog.title");
	}

	protected String getCommandName()
	{
		return TypoScriptBundle.message("newfile.command.name");
	}

	protected String getActionName(PsiDirectory directory, String newName)
	{
		return TypoScriptBundle.message("newfile.menu.action.text");
	}

	@NotNull
	protected PsiElement[] doCreate(String newName, PsiDirectory directory)
	{
		PsiFile file = createFileFromTemplate(directory, newName, TypoScriptTemplatesFactory.NEWFILE_FILE_NAME);
		PsiElement child = file.getLastChild();
		return child != null ? new PsiElement[]{file, child} : new PsiElement[]{file};
	}
}
