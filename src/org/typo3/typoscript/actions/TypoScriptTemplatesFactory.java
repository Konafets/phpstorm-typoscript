package org.typo3.typoscript.actions;

import com.intellij.ide.fileTemplates.FileTemplate;
import com.intellij.ide.fileTemplates.FileTemplateManager;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.psi.PsiDirectory;
import com.intellij.psi.PsiFile;
import com.intellij.psi.PsiFileFactory;
import com.intellij.util.IncorrectOperationException;
import org.jetbrains.annotations.NonNls;
import org.typo3.typoscript.fileTypes.TypoScriptFileType;

import java.util.Properties;

/**
 * Created with IntelliJ IDEA.
 * User: sok
 * Date: 08.04.12
 * Time: 12:55
 * To change this template use File | Settings | File Templates.
 */
public class TypoScriptTemplatesFactory
{
	public static final String NEWFILE_FILE_NAME = "TypoScript.ts";
	public static final String TS_HEADER_NAME = "TypoScript File Header.ts";

	private static final Logger log = Logger.getInstance("TypoScript.TemplatesFactory");

	public static PsiFile createFileFromTemplate(final PsiDirectory directory,
											 final String name,
											 String fileName,
											 String templateName,
											 @NonNls String... params) throws IncorrectOperationException
	{
		log.debug("createFromTemplate: dir: " + directory + ", filename: " + fileName);

		final FileTemplate template = FileTemplateManager.getInstance().getTemplate(templateName);

		Properties properties = new Properties(FileTemplateManager.getInstance().getDefaultProperties());

		String text;

		try
		{
			text = template.getText(properties);
		}
		catch (Exception e)
		{
			throw new RuntimeException("Unable to load template for " + FileTemplateManager.getInstance().internalTemplateToSubject(templateName), e);
		}

		final PsiFileFactory factory = PsiFileFactory.getInstance(directory.getProject());

		log.debug("Create file from ext");
		final PsiFile file = factory.createFileFromText(fileName, TypoScriptFileType.TYPOSCRIPT_FILE_TYPE, text);

		log.debug("Adding file to directory");
		return (PsiFile) directory.add(file);
	}
}
