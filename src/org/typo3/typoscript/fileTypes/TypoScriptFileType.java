package org.typo3.typoscript.fileTypes;

import com.intellij.openapi.vfs.CharsetToolkit;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.openapi.vfs.encoding.EncodingManager;
import org.typo3.typoscript.TypoScriptBundle;
import org.typo3.typoscript.TypoScriptLanguage;
import com.intellij.openapi.fileTypes.LanguageFileType;
import org.typo3.typoscript.icons.TypoScriptIcons;

import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.nio.charset.Charset;

public class TypoScriptFileType extends LanguageFileType
{
	public static final TypoScriptFileType TYPOSCRIPT_FILE_TYPE = new TypoScriptFileType();

	@NonNls
	public static final String DEFAULT_ASSOCIATED_EXTENSION = "ts";
	@NonNls
	public static final String DOT_DEFAULT_EXTENSION = "." + DEFAULT_ASSOCIATED_EXTENSION;

	private TypoScriptFileType()
	{
		super(TypoScriptLanguage.INSTANCE);
	}

	@NotNull
	@NonNls
	public String getDefaultExtension()
	{
		return DEFAULT_ASSOCIATED_EXTENSION;
	}

	@NotNull
	@NonNls
	public String getName()
	{
		return TypoScriptBundle.message("typoscript.filetype.name");
	}

	@NotNull
	public String getDescription()
	{
		return TypoScriptBundle.message("typoscript.filetype.description");
	}

	@Nullable
	public Icon getIcon()
	{
		return TypoScriptIcons.FILE_TYPE_ICON_16;
	}

	public String getCharset(@NotNull VirtualFile file, final byte[] content) {
		Charset charset = EncodingManager.getInstance().getDefaultCharsetForPropertiesFiles(file);
		String defaultCharsetName = charset == null ? CharsetToolkit.getDefaultSystemCharset().name() : charset.name();
		return defaultCharsetName;
	}
}
