package org.typo3.typoscript.fileTypes;

import com.intellij.openapi.fileTypes.FileTypeFactory;
import com.intellij.openapi.fileTypes.FileTypeConsumer;
import org.jetbrains.annotations.NotNull;

public class TypoScriptFileTypeFactory extends FileTypeFactory
{
	@Override
	public void createFileTypes(@NotNull FileTypeConsumer fileTypeConsumer)
	{
		fileTypeConsumer.consume(TypoScriptFileType.TYPOSCRIPT_FILE_TYPE, TypoScriptFileType.DEFAULT_ASSOCIATED_EXTENSION + ";t3s;t3c");
	}
}
