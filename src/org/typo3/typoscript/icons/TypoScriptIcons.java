package org.typo3.typoscript.icons;

import com.intellij.openapi.util.IconLoader;

import javax.swing.*;

public interface TypoScriptIcons
{
	Icon FILE_TYPE_ICON_16 = IconLoader.getIcon("/org/typo3/typoscript/images/typoscript16.png");
}
